# Definition: [01][02]
    * kleine, voneinander unabhängige Services +
    * nach Funktionen aufgesplitted +
    * kommunizieren über einfache Protokolle, meist HTTP +
    * unabhängig voneinander deployed und entwickelt +
    * dezentrale Datenhaltung +
    * können in unterschiedlichen Sprachen implementiert sein +
    * einzeln skalierbar +
    * voll automatisiertes deployment +
    * Full Stack (Abgrenzung zu SOA) +
    * Bottom up +

# Docker & Co: vergleichen
# Termin im November

# GLIEDERUNG
    # Motivation
        - Microservices immer populärer, aber wirklich neu? SOA exitsiert bereits
    # Definition
        - Microservice: "minimaler, unabhängiger Prozess der über Nachrichten interagiert" [06]
            - einzeln deploybar, jeweils Full-Stack Anwendung
            - vollautomatisiertes deployment
        - Microservice Architecture: "verteilte Anwendung, dessen Module alle Microservices sind" [06]
            - nach Business Capabilities aufgeteilt
            - Kommunikation über einfache Protokolle, meist HTTP
            - verschiedene Sprachen möglich
            - Bottom up Prinzip
    # Abgrenzung SOA
        - SOA-Services meist Monolithen im gesamten [04]
        - Kommunikation über HTTP

    # Beispiele
        # Docker
    # Vor & Nachteile
    # Fazit
# Abgrenzung:
    SOA:
        Kommt von SOA, SOA meint aber zu viel [02]
        SOA bezieht sich mehr auf die gesamte IT eines Unternehmens, die Teile in Services zu fassen und das Unternehmen flexibler zu gestalten. MS beziehen sich auf eine Anwendung, in der einzelne Teile von verschiedenen Teams
        gebaut werden. [04]
        Microservices sind unabhängig deploybar, wobei Services meist Deployment-Monolithen sind [04]
    Altes UNIX Prinzip: "Make each program do one thing well. To do a new job, build a fresh rather than complicate old programs by adding new "features."" [03]


Literatur:
#01 Microservices: Nur ein weiteres Konzept in der Softwarearchitektur oder mehr?
* Abgrenzung zu monolithischer Architektur
* Nicht nur Softwarearchitektur, auch strukturelle Änderungen:
    * Teams werden nach Funktionen aufgeteilt, nicht mehr nach Frontend, Backend
    * Entwickler sind auch Maintainer ("You build it, you run it", Amazon)
* "Regeln" denen MS folgen:
    * "Smart Endpoints and dumb Pipes"
    * "You build it, you run it"
    * "Design for failure"
* Stellt Vorteile von MS dar:
    *

#02 http://martinfowler.com/articles/microservices.html
    * Definition
    * SOA:
        * Kommt von SOA, allerdings ist SOA zu allumfassender Begriff


# heise refrenzen

#03 BSTJ 57: 6. July-August 1978: UNIX Time-Sharing System: Forward. (McIlroy, M.D.; Pinson, E.N.; Tague, B.A.)
    https://archive.org/details/bstj57-6-1899


#04 http://www.oracle.com/technetwork/issue-archive/2015/15-mar/o25architect-2458702.html
Microservices and SOA


#05 An Emergent Micro-Services Approach to Digital Curation Infrastructure, 2010
"The new UC3 approach to digital curation infrastructure is based on the idea of devolving necessary function
into a set of independent, but interoperable, micro-services that embody curation
values and strategies. Since each of the services is small, they are collectively easier to
develop, deploy, maintain, and enhance"

#06 Microservices: yesterday, today, and tomorrow


https://stackoverflow.com/questions/25501098/difference-between-microservices-architecture-and-soa

applikation vs anwendung (wort)

http://delivery.acm.org/10.1145/180000/176805/p143-kautz.pdf?ip=141.76.112.190&id=176805&acc=ACTIVE%20SERVICE&key=2BA2C432AB83DA15%2E71FE45B894D107A6%2E4D4702B0C3E38B35%2E4D4702B0C3E38B35&CFID=702919486&CFTOKEN=67802605&__acm__=1481469367_feca8cfd6881f9ac9eed9e76531d027c

http://delivery.acm.org/10.1145/2950000/2948985/p42-killalea.pdf?ip=141.76.112.190&id=2948985&acc=ACTIVE%20SERVICE&key=2BA2C432AB83DA15%2E71FE45B894D107A6%2E4D4702B0C3E38B35%2E4D4702B0C3E38B35&CFID=702919486&CFTOKEN=67802605&__acm__=1481469355_63c68cc352cb8e91890b2ea6ec5f4969

















Struktur:
    Einführung
        Überblick über die Struktur geben
    Microservices
        Vgl. monolithischer Ansatz
        Definition
            Aufbau MS
            Kommunikation
            Entwicklung von MS Architektur
        Vorteile
        Nachteile
    Abgrenzung
        Definition SOA
        Microservices aus SOA, Scrum, DevOps, neue Technologien, Cloud Computing herleiten
        Komponentenansatz
    Beispiele
        Applikationen
        Frameworks zur Realisierung
    Fazit

SOA
    Interoperabilität sehr wichtig, auch unternehmensübergreifend → kein Deploymentmonolith
    Orchestrierung und Choreography möglich
    Grob-Granular
Microservices
    sind fine-grained SOA
    verteiltes Rechnen ohne Overhead (XML, SOAP)
    technologische Entwicklung: Virtualisierung, Cloud Computing
    keine starren Verknüpfungen
Komponentenansatz
    dotNET, Java ESBeans
