\documentclass{llncs}
\usepackage{makeidx}  % allows for indexgeneration
\usepackage[utf8]{inputenc}
\usepackage[ngerman]{babel}
\usepackage[final]{microtype}
\usepackage[fixlanguage]{babelbib}
\usepackage{graphicx}
\usepackage{url}

%citation
\newcommand\q[1]{\glqq #1\grqq}

\bibliographystyle{splncs03}

%Code
\usepackage{listings}
\usepackage{color}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

\begin{document}
\pagestyle{headings}  % switches on printing of running heads
\mainmatter          % for the preliminaries
\title{Microservices - Die Revolution in der Anwendungsentwicklung?}
\author{Sönke Huster}
\institute{Technische Universität Dresden, Dresden, Germany,
\email{jan\_soenke.huster@tu-dresden.de}}

\maketitle              % typeset the title of the contribution
\begin{abstract}
    Seit 2014 steigt die Popularität der Microservice-Architektur (MSA) stetig. Das Konzept wird als \q{revolutionär} bezeichnet.
    Bei einer ersten Betrachtung weist die MSA starke Ähnlichkeiten mit der seit langem bekannten Serviceorientierten Architektur auf.

    Dieser Artikel beschäftigt sich mit dem Konzept der MSA und der Frage,
    ob diese wirklich eine revolutionäre Neuerung darstellt.
    Dazu wird ein Überblick über vorhandene Definitionen gegeben sowie
    Bezüge zu vorherigen Architekturmodellen hergestellt.
    Abschließend werden Frameworks zur Realisierung der MSA und eine Beispielanwendung vorgestellt.
\end{abstract}

\section{Einführung}
Seit 2014 wird der Begriff \q{Microservice-Architektur} (MSA) immer populärer \cite{googletrendsmicroservices}.
%Diverse Artikel beschäftigen sich mit der Umstellung bereits existierender Software auf die \q{neue} Architektur \cite{balalaie2015migrating} \cite{abrams2010emergent}.
Einige Autoren bezeichnen diese gar als \q{revolutionär} \cite{montesimicroservices}.

Bei der MSA wird Software in möglichst kleine Bestandteile aufgeteilt -- sog. \emph{Microservices}.
Diese kommunizieren untereinander und bilden als potentiell dezentraler, vernetzter Komplex die Anwendung.
Dieses Prinzip ähnelt auf den ersten Blick stark dem der Serviceorientierten Architektur (SOA), welche bereits seit 1996 verwendet wird \cite{schulte1996service}.
Somit stellt sich die Frage, ob es sich bei der MSA wirklich um einen völlig neuen Ansatz zur Softwareentwicklung handelt oder bloß ein bereits bestehendes Konzept neuartig aufgerollt wird.

Um dies zu beantworten, werden zunächst verschiedene Ansätze zur Softwarearchitektur vorgestellt, danach wird ein Überblick über den Begriff der MSA gegeben sowie Vor- und Nachteile aufgezeigt.
Abschließend wird eine beispielhafte Anwendung vorgestellt, welche die MSA nutzt, sowie Frameworks präsentiert, die die Entwicklung von Microservices erleichtern.

\section{Historie}
\subsection{Monolithische Architektur}
% Vgl. monolithischer Ansatz
Um die MSA zu definieren, wird diese oft mit dem monolithischen Ansatz verglichen.
In einer monolithischen Software sind alle Module einer Anwendung voneinander abhängig und miteinander verwoben \cite{stephens2015beginning}.
Sie teilen sich Ressourcen und die Applikation kann nur als ein Gesamtpaket bestehen.

Dieser monolithische Ansatz weist einige Nachteile auf. So gestaltet sich die Wartung eines komplexen und fortwährend weiterentwickelten, sog.n \emph{Monolithen} als aufwändig.
Da die Anwendung aus nur einem Paket besteht, ist ein Neustart oft selbst nach einer minimalen Änderung des Codes erforderlich.
Somit wird auch die Wartung stark erschwert.
Im Hinblick auf die Skalierung besteht bei dem monolithischen Ansatz ebenso ein Problem:
Da die Anwendung nicht modularisiert ist, kann die Skalierung nur im Gesamten erfolgen.
%, also die Leistungssteigerung durch das Hinzufügen oder Entfernen weiterer Server,
Es ist also nicht möglich, oft genutzten Funktionalitäten mehr Rechenleistung zur Verfügung zu stellen als jenen, welche weniger genutzten werden.
Außerdem gestaltet sich das Wiederverwenden von Anwendungsteilen als schwierig, da diese in der Regel nicht unabhängig von der Gesamtanwendung funktionieren.

\subsection{Komponentenbasierte Entwicklung}
Eine bessere Wiederverwendbarkeit schafft die Komponentenbasierte Entwicklung: Sie weist den Ansatz auf, Software in Komponenten zu unterteilen, welche miteinander kommunizieren \cite{hasselbring2002component}.
Diese sind voneinander unabhängig und halten keine gemeinsamen Daten, sodass die Entwicklung der Komponenten separat erfolgen kann.
So muss eine bestimmte Funktionalität nur einmal programmiert werden und kann später in anderen Anwendungen wieder eingesetzt werden.

Da sie allerdings gemeinsam in einem Container verteilt werden, besteht im Hinblick auf die Skalierung das gleiche Problem wie bei der monolithischen Architektur.

\subsection{Serviceorientierte Architektur}
% Definition SOA
Unter anderem dieses Problem löst das serviceorientierte Architekturparadigma.
Es beschreibt \q{die Strukturierung und Nutzung verteilter Funktionalität} \cite{mackenzie2006reference}.
Hier werden verschiedene Funktionalitäten über definierte Schnittstellen durch Services bereitgestellt.
Diese Services werden nach fachlichen Funktionalitäten gegliedert.
Services können miteinander kommunizieren, wobei es zwei verschiedene Ansätze gibt: Orchestrierung und Choreographie.
Für der Orchestrierung wird ein zentraler Service benötigt, welcher Anfragen an Services stellt und den Gesamtprozess verwaltet.
Im Gegensatz dazu gibt es bei der Choreographie keine Zentralisierung, hier kommunizieren die Services miteinander \cite{dragoni2016microservices}.

SOA fördert ebenso die Wiederverwendung von Softwarekomponenten und hat die Verringerung der Komplexität einer Anwendung als Ziel.
Insbesondere die Interoperabilität, auch unternehmensübergreifend, steht im Fokus der SOA.
So können Unternehmen Services nach außen öffnen, sodass der Zugriff für andere auf diese ermöglicht wird.

Durch die verteilten und unabhängigen Services wird ebenso das Problem der Skalierung gelöst: Diese können auf verschiedenen Servern laufen und so einzeln skaliert werden.

\section{Microservices}
Es existiert bisher keine einheitliche Definition der MSA.
Lediglich wenige Publikationen charakterisieren dieses Konzept \cite{fowler2014microservices} \cite{dragoni2016microservices}.

\subsection{Definition Microservices}
\textsc{James Lewis} und \textsc{Martin Fowler} haben 2014 eine erste Definition der MSA entwickelt \cite{fowler2014microservices}.
Diese präzisieren \textsc{Nicola Dragoni} et. al in \cite{dragoni2016microservices}.
Sie definieren den einzelnen Microservice als einen \q{minimalen und unabhängigen Prozess, der über Nachrichten interagiert}.
Die MSA wird dort als verteilte Anwendung beschrieben, bei deren Modulen es sich ausschließlich um Microservices handelt.

% Aufbau von MS 
Nach beiden Definitionen wird eine Anwendung in möglichst kleine Services aufgeteilt, welche vollständig unabhängig und isoliert voneinander agieren.
Die Services sind in der Theorie strikt nach den Funktionen der Applikation getrennt, von denen in einem Service jeweils genau eine umgesetzt werden soll.
Wesentlich ist, dass die einzelnen Microservices jeweils \emph{Full-Stack-Anwendungen} sind, wodurch eine dezentrale Datenhaltung bedingt wird.
Dies ermöglicht, in den einzelnen Microservices jeweils vollständig voneinander unabhängige Technologien nutzen zu können.
Deshalb können die einzelnen Services in unterschiedlichsten Sprachen implementiert werden. Außerdem ist die Nutzung von verschiedensten Datenmodelle möglich.

\subsection{Microservices-Architektur}
% Kommunikation
Die Services selbst kommunizieren über einfache Protokolle wie z.B. HTTP. Ein Grundsatz dabei lautet \q{Smart Endpoints, dumb Pipes}.
So findet keine zentrale Orchestrierung statt. Also wird der Prozessfluss nicht durch nur einen Service gesteuert.
\begin{figure}
    \centering
    \includegraphics[width=0.6\linewidth,keepaspectratio]{abbildungen/distributed-ms-architecture.png}
    \caption{Microservices mit dezentraler Datenhaltung in einer Microservice-Architektur}
\end{figure}

% Entwicklung von MS Architektur
Die Entwicklung erfolgt in der Regel nach dem \q{Bottom Up}-Prinzip:
Dieses beginnt damit, dass die Schnittstellen einzelner Microservices definiert und implementiert werden \cite{Kautz:1994:BDS:176789.176805}.
Anschließend wird daraus die Gesamtanwendung zusammengestellt.
So kann die Entwicklung der Services durch die Atomisierung und Isolation unabhängig voneinander erfolgen.
Auch das Deployment eines Services, welches in der Regel komplett automatisiert stattfindet,
ist unabhängig von dem Deployment der anderen Services.

Durch die Dezentralität muss die Gesamtanwendung nach dem \q{Design for Failure}-Prinzip entworfen sein,
sodass diese idealerweise auch bei dem Ausfall einzelner Services weiterhin funktioniert \cite{fowler2014microservices}.
Bei einigen Befürwortern von Microservices gilt außerdem die Strategie \q{You build it, you run it},
%Dieses Prinzip wird beispielsweise von Amazon  und Netflix verfolgt.
so sind die Entwickler während des gesamten Lebenszyklus des Services für diesen verantwortlich \cite{gray2006conversation}.

% Technologische Entwicklung führte zu MS
Ein Faktor der zur Entwicklung der MSA führte, ist die fortschreitende technologische Entwicklung im Bereich des Cloud-Computing.
Vermehrte Virtualisierungsmöglichkeiten sowie die simple Verfügbarkeit von leistungsstarken Hardwareressourcen sind Vorraussetzungen für die Nutzung der Vorteile dieser Architektur.
Virtualisierung beschreibt die Abstraktion von Ressourcen für den Nutzer.
Diese Abstraktion ermöglicht es \emph{Infrastructure-as-a-Service}-Anbietern,
wie beispielsweise \emph{Amazon Web Services} (AWS), dynamisch Ressourcen bereitzustellen, welche innerhalb von Sekunden abgerufen werden können.
%Außerdem existieren Virtualisierungslösungen auf Betriebssystemebene wie beispielsweise \emph{Docker}, sodass Microservices komplett in sog. Container isoliert werden können.
So können Microservices ab einem Lastschwellwert automatisch auf neue Instanzen repliziert oder wieder entfernt werden.
Ein sog. \emph{Load-Balancer} verteilt die Last gleichmäßig auf die verfügbaren Instanzen.
Somit wird auf Lastenspitzen sowie -tiefen dynamisch und komplett automatisiert reagiert \cite{caron:hal-00668713}.

% Agilität
Ebenso erzeugte die Beliebtheit der agilen Softwareentwicklung das Bedürfnis nach schneller und flexibler Programmierung, wie sie letztendlich durch die MSA begünstigt wird.
Auch die Umsetzung des \q{DevOps}-Prinzips kann am effektivsten mit der MSA erreicht werden.
Dabei geht es darum, die Softwarequalität zu steigern, indem Entwickler und Administratoren enger zusammenarbeiten.
So sollen Änderungen häufiger ausgerollt werden, was durch eine Automatisierung des Testens und Deplyoments erreicht wird.
Doch auch die laufende Anwendung soll kontinuierlich automatisiert überwacht werden, sodass Fehler und Performance-Abweichungen sofort erkannt werden können.
Da Teams bei der MSA über den gesamten Lebenszyklus eines Services für diesen verantwortlich sind, begünstigt dies das \q{DevOps}-Prinzip stark.

% MS herleiten
Die MSA ist aus den Konzepten der SOA entstanden und präzisiert diese:
So bezeichnet das Unternehmen Netflix, das für mehr als 30\% des nordamerikanischen Internetverkehrs verantwortlich ist, ihre MSA als feingranulare SOA \cite{netflixservices2013}.
\textsc{Torsten Winterberg}, Oracle ACE Direktor, beschrieb Microservices als \q{the kind of SOA we have been talking about for the last decade} \cite{rhubart2015microservices}.

\subsection{Vor- \& Nachteile}
% Vorteile
Die MSA bringt diverse Vorteile mit sich:
Die Wartung ist wegen der Größe und der geringen Komplexität eines einzelnen Services einfacher als bei herkömmlichen Monolithen.
Aufgrund von Automatisierung und Unabhängigkeit ist es möglich, einzelne Änderungen innerhalb von Sekunden zu deployen.
So können Teilbestandteile einfach gepatcht und Anwendungskomponenten asynchron weiterentwickelt werden \cite{dragoni2016microservices}.
Außerdem führt diese Unabhängigkeit in Kombination mit Fehlertoleranz zu einer besseren Verfügbarkeit \cite{reese2011awsoutage}.
Auch das Testen eines einzelnen Services ist durch die Isolation einfacher zu realisieren.

% Nachteile
Es zeigen sich allerdings auch Nachteile: %Die Netzwerklatenz ist im Vergleich zur Speicherlatenz höher.
%Dies fällt bei der MSA schwerer ins Gewicht, als bei monolithischen Applikationen, da Microservices untereinander über das Netzwerk kommunizieren, bevor der User eine Antwort erhält.
% Da Microservices in der Regel über das Netzwerk kommunizieren, führt dies zu einer schlechteren Leistung der Anwendung.
Im Allgemeinen führt die Netzwerkkommunikation bei verteilten Systemen zu einem höheren Sicherheitsrisiko. Auch ist sie deutlich langsamer als \emph{In-Process}-Kommunikation.
Es müssen spezielle Mechanismen implementiert werden, um die Datensicherheit trotz der Netzwerkkommunikation zu gewährleisten \cite{sun2015security}.
Die durch das \q{Design for Failure}-Prinzip geforderte Fehlertoleranz ist schwer zu implementieren, weshalb die Komplexität der gesamten Anwendung enorm steigt.
Zuletzt gestalten sich Integrationstests, gerade wenn es viele Verbindungen zwischen einzelnen Services gibt und die gesamte Anwendung sehr groß ist, als schwierig \cite{dragoni2016microservices}.

\subsection{Anwendungsbeispiele}
Es gibt einige Anwendungen, welche die MSA nutzen. Dazu gehören unter Anderem die Musikplattform SoundCloud, der Webshop des Otto-Versands, Amazon und der Video-on-Demand-Anbieter Netflix, dessen Architektur im Folgenden etwas genauer dargestellt wird.

\subsubsection{Netflix}
ist eines der bekanntesten Unternehmen, welches bei der eigenen Anwendung auf die MSA setzt.
Ausschlaggebend für die Umstellung war die bessere Skalierbarkeit und die höhere Ausfallsicherheit einer microservicebasierten Anwendung.
Die zuvor monolithische Anwendung wurde von über 100 Microservices ersetzt, welche bei AWS laufen.
Diese kommunizieren untereinander über HTTP nach dem REST-Paradigma.

\begin{figure}
    \centering
    \includegraphics[width=0.6\linewidth,keepaspectratio]{abbildungen/netflix_microservices.png}
    \caption{Übersicht der einzelnen Microservices mit Verbindungen untereinander in Netflix' MSA \cite{cockroft2013cloudarchitecture}.}
\end{figure}

Netflix nutzt das sog. \emph{API-Gateway}-Pattern, ähnlich dem \q{Facade}-Pattern, welches einen Einstiegspunkt für das gesamte System schafft.
Wie in der Abb. 2 zu sehen ist, gibt der sog. API-Gateway die Anfragen an die zugehörigen Microservices weiter.
Die Microservices sind einzeln skalierbar. So stellt ein einzelner Knoten in Abb. 2 mehrere Instanzen des selben Services dar.
Services werden autonom in eigenen Teams entwickelt, die über den gesamten Lebenszyklus verantwortlich sind.
%Zusätzlich zur Architekturänderung gab es auch strukturelle Änderungen bei der Entwicklung.
%Zunächst waren die Entwickler nach ihren Tätigkeiten gruppiert, mittlerweile sind für jeden Microservice eigene Teams über den gesamten Lebenszyklus verantwortlich.
%So wird jeder Service autonom weiterentwickelt.

Ein Großteil der Microservices wurden in Java realisiert, es werden aber auch Scala, JavaScript, Dart, C++, Groovy, Python und Ruby genutzt \cite{toth2015netflixarchitektur}.
Das Testen und das Deployment wurden komplett automatisiert, außerdem wurden zahlreiche Tools gebaut und veröffentlicht\footnote{https://netflix.github.io/}, welche für die kontinuierliche Überwachung, Kommunikation und Entwicklung der eigenen Microservice-Infrastruktur genutzt werden.

\section{Frameworks}
Netflix hat eine Menge Frameworks gebaut, welche die Entwicklung einer MSA vereinfachen. Beispielhaft dafür ist Hystrix, dessen Verwendung die Fehlertoleranz einzelner Services erhöhen soll.
Außerdem ist die Virtualisierungslösung Docker als Deploymentlösung für MSA von großer Beliebtheit.
\subsubsection{Docker}
ist eine Virtualisierungslösung auf Betriebssystemebene \cite{dockerdocs}.

Mit Docker lassen sich sog. \emph{Docker-Images} erstellen. Innerhalb dieser wurde die gewünschte Software installiert und konfiguriert (siehe Abb. 3).
Im Gegensatz zu traditionellen virtuellen Maschinen (VM), welche Virtualisierung auf Hardware-Ebene ermöglichen, teilen sich Docker-Images den Betriebssystem-Kernel.
Dies macht Docker-Images deutlich leichtgewichtiger und performanter als VM.

\begin{figure}
    \caption[Ein Beispiel]{Ein Beispiel\footnote{\url{https://github.com/kstaken/dockerfile-examples}} für ein \emph{Dockerfile},
    welches ein \emph{Docker-Image} mit dem \emph{Apache}-Webserver erstellt und auf \emph{Ubuntu 12.04} basiert.}
    \begin{lstlisting}
    FROM ubuntu:12.04
    RUN apt-get update && apt-get install -y apache2 && apt-get clean && rm -rf /var/lib/apt/lists/*
    EXPOSE 80
    CMD ["/usr/sbin/apache2", "-D", "FOREGROUND"]
    \end{lstlisting}
\end{figure}

In sog. \emph{Dockerfiles} kann definiert werden, wie ein \emph{Docker-Image} gebaut wird.
Dies ermöglicht einen einfachen Überblick über die zu installierende Software. Desweiteren können diese durch ihre geringe Größe einfacher verbreitet und gut versioniert werden.
Außerdem kann ein \emph{Docker-Image} als Basis angegeben werden \cite{turnbull2014docker}.

Lässt sich das erstellte Image nun mit Docker starten, so spricht man von einem \emph{Docker-Container}.

In Kombination mit der MSA vereinfacht dies das Deployment eines einzelnen Microservices enorm:
Jeder Microservice kann als einzelner, leichtgewichtiger \emph{Docker-Container} gebaut und verteilt werden.
Durch die Portabilität eines \emph{Docker-Containers} können Deployment und Skalierung rasch erfolgen.
Da die Ausführungsumgebung innerhalb eines Containers isoliert von anderen ist, können Services bei geringer Auslastung einfach den selben Server gemeinsam nutzen.

Daher ist Docker in Kombination mit der MSA sehr beliebt \cite{urra2016distributed}.

\subsubsection{Hystrix}
ist ein von Netflix entwickelter, sog. \emph{Circuit-Breaker}, welcher auf Github veröffentlicht wurde \footnote{\url{https://github.com/Netflix/Hystrix}}.
Diese unterstützen den Entwickler bei der Umsetzung des \q{Design for Failure}-Prinzips.

Da Microservices zum Einen untereinander, zum Anderen mit externen Services kommunizieren, kann der Ausfall eines Services schnell zu einem Fehler im eigenen Service führen.
Möglicherweise hängen von diesem wiederum andere Services ab, sodass sich bereits ein kleiner Fehler ausbreiten und die gesamte Anwendung zum Absturz bringen kann. Dies nennt man \emph{Cascade Failure}.

Hystrix ist bei der Isolation von externen Aufrufen behilflich, unterbindet diese im wiederholten Fehlerfall und stellt Fallback Lösungen bereit.
Dazu müssen diese Aufrufe in einen Hystrix-Command gekapselt werden.
Wird ein definierter Schwellwert von Fehlversuchen erreicht, kann die implementierte Fallback-Aktion direkt ausgeführt werden.
In regelmäßigen Intervallen wird getestet, ob der externe Service wieder erreichbar ist. Falls dies zutrifft, werden Aufrufe an diesen wieder zugelassen.
Alle eingetretenen Ereignisse werden in einem kontinuierlichen Stream veröffentlicht und unterstützt somit auch bei der kontinuierlichen Überwachung des Systems \cite{landwehrhystrix}.

\section{Fazit}
Die MSA stellt keine Revolution in der Anwendungsentwicklung dar. Ihre Entstehung entsprang aus der SOA, gestiegenen Anforderungen sowie technischer Innovation, welche neuartige Entwicklung ermöglichte.
Es nicht verwunderlich, dass Internet-Giganten wie Netflix oder Amazon maßgeblich zu dieser Evolution beigetragen haben -- beide Unternehmen wollen ihre Anwendung sehr schnell und flexibel an Trends anpassen und haben extreme Anforderungen an die Skalierbarkeit ihrer Dienste.
Dies zeigt aber auch, dass die MSA nicht für jeden Anwendungsfall die richtige Lösung ist:
Wendet man sie an, steigt die Komplexität durch das verteilte, dezentrale System enorm.

Die MSA ist relativ neu: die bekannten Anwendungen sind große Webplattformen und unterliegen starken Nutzungsschwankungen.
Die Nutzung dieser Architektur ist auch nur in diesem Fall sinnvoll. Unternehmen, welche dieser Art der Dynamik nicht ausgesetzt sind, wird der Nachteil der Komplexität gegenüber den Vorteilen der Skalierbarkeit und der verbesserten Wartung höher wiegen.

\bibliography{sources}
\clearpage
\addtocmark[2]{Author Index} % additional numbered TOC entry
\renewcommand{\indexname}{Author Index}
\printindex
\clearpage
\end{document}
